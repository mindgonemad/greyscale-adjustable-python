# Adjustable Greyscale

Allows setting of end points on 9 step scale for matching of values, giving you a reference for mixing all the intermediate values. 

## Getting Started

Adjustable greyscale built using Pythonista UI on iOS.

### Prerequisites

Requires Pythonista on iOS

### Installing and running

Copy files to Pythonista or access files using 'Open'.
Open the Greyscale.py file in Pythonista and press the Play button.
Move the sliders to the required end points and the middle steps will evenly distribute to match. 

## Built With

* [Pythonista](https://omz-software.com/pythonista/)
* [Pythonista UI](https://omz-software.com/pythonista/docs/ios/ui.html)

## Versioning

I haven't figured out how I am going to version it yet.

## Authors

* **Craig Hofman** - [Fragile Ninja](http://fragile.ninja)

## License

I haven't decided licensing, please get in contact if you wish to use this code.

## Acknowledgments

