import ui


# White
def adjust_9(sender):
    global box_1_value, box_9_value
    box_9_value = (sender.value / 2) + 0.5
    
    update_int_blocks(box_1_value, box_9_value)
  
# Black
def adjust_1(sender):
    global box_1_value, box_9_value
    box_1_value = sender.value / 2
    
    update_int_blocks(box_1_value, box_9_value)
  
# Update intermediate blocks
def update_int_blocks(black_val = 0, white_val = 1):
    global box_1_value
    global box_9_value
    box_1_value = black_val
    box_9_value = white_val
    value_range = box_9_value - box_1_value
    step = value_range / 8
    
    # White
    box_9.background_color = (box_9_value)
    label_9.text = str(round(box_9_value * 100))
    
    # Inters
    box_8_value = box_1_value + (step * 7)
    box_8.background_color = (box_8_value)
    label_8.text = str(round(box_8_value * 100))
    box_7_value = box_1_value + (step * 6)
    box_7.background_color = (box_7_value)
    label_7.text = str(round(box_7_value * 100))
    box_6_value = box_1_value + (step * 5)
    box_6.background_color = (box_6_value)
    label_6.text = str(round(box_6_value * 100))
    box_5_value = box_1_value + (step * 4)
    box_5.background_color = (box_5_value)
    label_5.text = str(round(box_5_value * 100))
    box_4_value = box_1_value + (step * 3)
    box_4.background_color = (box_4_value)
    label_4.text = str(round(box_4_value * 100))
    box_3_value = box_1_value + (step * 2)
    box_3.background_color = (box_3_value)
    label_3.text = str(round(box_3_value * 100))
    box_2_value = box_1_value + (step * 1)
    box_2.background_color = (box_2_value)
    label_2.text = str(round(box_2_value * 100))
    
    # Black
    box_1.background_color = (box_1_value)
    label_1.text = str(round(box_1_value * 100))
    
 

v = ui.load_view('Greyscale')

# If you use a square canvas, the same image can be used in portrait and landscape orientation.
w, h = ui.get_screen_size()
canvas_size = min(w, h) - 64
v.frame = (0, 0, canvas_size, canvas_size)
v.size_to_fit()
v.background_color = (0.5)


# White start
box_9 = v['box_9']
label_9 = v['label_9']
#box_9_value = 1
box_9.background_color = (1)

# Intermediate start
box_2 = v['box_2']
label_2 = v['label_2']
box_3 = v['box_3']
label_3 = v['label_3']
box_4 = v['box_4']
label_4 = v['label_4']
box_5 = v['box_5']
label_5 = v['label_5']
box_6 = v['box_6']
label_6 = v['label_6']
box_7 = v['box_7']
label_7 = v['label_7']
box_8 = v['box_8']
label_8 = v['label_8']

# Black start
box_1 = v['box_1']
label_1 = v['label_1']
#box_1_value = 0
box_1.background_color = (0)

# Initial run
update_int_blocks()




v.present('sheet')
